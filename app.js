const express = require('express'),
  app = express(),
  config = require("./configs/config"),
  register = require('./register'),
  login = require("./login"),
  usuarios = require("./routes/usuarios"),
  sets = require('./routes/sets'),
  cartas = require('./routes/cartas'),
  protectRoute = require('./routes/rutasProtegidas'),
  mazos = require('./routes/mazos'),
  favoritos = require('./routes/favoritos'),
  comparar = require('./routes/comparar'),
  logout = require("./routes/logout"),
  simbolos = require("./routes/simbolos");


app.use(express.urlencoded({ extended: false }));
app.use(express.json());

//---DESCOMENTAR SI SE QUIERE USAR LA BD CON SEQUELIZE---
require('./model/dbSequelize');

//RUTAS PUBLICAS
app.use('/simbolos', simbolos);
app.use("/register", register);
app.use("/login", login);
app.use('/sets', sets);
app.use('/cartas', cartas);

//RUTAS AUTENTICADAS
app.use("/usuario", protectRoute, usuarios);
app.use('/mazos', protectRoute, mazos);
app.use('/favoritos', protectRoute, favoritos);
app.use('/comparar', protectRoute, comparar);
app.use('/logout', protectRoute, logout);


app.listen(config.port, () => {
  console.log(`Example app listening at http://localhost:${config.port}`);
  //DESCOMENTAR SI SE QUIERE ACTUALIZAR LA BBDD
  // updatebbdd();
})

