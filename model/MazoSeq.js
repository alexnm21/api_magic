module.exports = (sequelize, type) => {
    return sequelize.define('mazos', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nombre: type.STRING,
        id_usuario: type.INTEGER
    });
}
