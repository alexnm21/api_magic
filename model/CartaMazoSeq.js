module.exports = (sequelize, type) => {
    return sequelize.define('cartas_mazo', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_mazo: {
            type: type.INTEGER,
            primaryKey: true
        },
        id_carta: {
            type: type.STRING,
            primaryKey: true
        }
    });
}
