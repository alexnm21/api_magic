module.exports = (sequelize, type) => {
    return sequelize.define('set', {
        id: {
            type: type.STRING,
            primaryKey: true,
        },
        code: type.STRING,
        name: type.STRING,
        set_type: type.STRING,
        card_count: type.INTEGER,
        fecha_lanzamiento: type.STRING,
        icon: type.STRING,
        parent_code: type.STRING
    });
}
