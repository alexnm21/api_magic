module.exports = (sequelize, type) => {
    return sequelize.define('usuarios', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nombre: type.STRING,
        apellidos: type.STRING,
        usuario: type.STRING,
        password: type.STRING,
        img_perfil: type.STRING,
        token: type.STRING
    });
}
