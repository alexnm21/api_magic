class respuestaError {

    constructor(code, message){
        this.code = code;
        this.message = message;
    }

}

module.exports = respuestaError;