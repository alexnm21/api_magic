module.exports = (sequelize, type) => {
    return sequelize.define('simbolos', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        simbolo: type.STRING,
        img: type.STRING,
        descripcion: type.STRING
    });
}