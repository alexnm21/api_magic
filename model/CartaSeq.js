module.exports = (sequelize, type) => {
    return sequelize.define('cartas', {
        id: {
            type: type.STRING,
            primaryKey: true
        },
        name_back: type.STRING,
        name_front: type.STRING,
        img_small: type.STRING,
        img_normal: type.STRING,
        img_large: type.STRING,
        // img_normal_back: type.STRING,
        // img_large_back: type.STRING,
        fecha_lanzamiento: type.STRING,
        mana_cost_front: type.STRING,
        mana_cost_back: type.STRING,
        cmc: type.INTEGER,
        typeline_back: type.STRING,
        typeline_front: type.STRING,
        power_back: type.INTEGER,
        power_front: type.INTEGER,
        descripcion_back: type.STRING, 
        descripcion_front: type.STRING,
        layout: type.STRING,
        flavor_text_back: type.STRING,
        flavor_text_front: type.STRING,
        rarity: type.STRING,
        precio_eur: type.STRING,
        toughness_back: type.INTEGER,
        toughness_front: type.INTEGER,
        code_set: type.STRING 
    });
}