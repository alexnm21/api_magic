module.exports = (sequelize, type) => {
    return sequelize.define('comparar', {
        id_carta: {
            type: type.STRING,
            primaryKey: true
        },
        id_usuario: {
            type: type.INTEGER,
            primaryKey: true
        }
    },{
        freezeTableName: true
    });
}
