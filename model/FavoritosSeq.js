module.exports = (sequelize, type) => {
    return sequelize.define('favoritos', {
        id_carta: {
            type: type.STRING,
            primaryKey: true
        },
        id_usuario: {
            type: type.INTEGER,
            primaryKey: true
        }
    });
}
