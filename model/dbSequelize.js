const Sequelize = require('sequelize');
require('dotenv').config();

const CardModel = require('./CartaSeq');
const SetModel = require('./SetSeq');
const UsuarioModel = require('./UsuarioSeq');
const MazoModel = require('./MazoSeq');
const CartaMazoModel = require('./CartaMazoSeq');
const FavoritosModel = require('./FavoritosSeq');
const CompararModel = require('./CompararSeq');
const SimboloModel = require('./SimboloSeq');

const connection = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST,
    dialect: 'mysql',
    define: {
        timestamps: false
    }
});

connection.sync({ force: false })
    .then(() => {
        console.log('Tablas Sincronizadas!');
    })

const Card = CardModel(connection, Sequelize);
const Set = SetModel(connection, Sequelize);
const Usuario = UsuarioModel(connection, Sequelize);
const Mazo = MazoModel(connection, Sequelize);
const CartaMazo = CartaMazoModel(connection, Sequelize);
const Favorito = FavoritosModel(connection, Sequelize);
const Comparar = CompararModel(connection, Sequelize);
const Simbolo = SimboloModel(connection, Sequelize);

module.exports = {
    Card,
    Set,
    Usuario,
    Mazo,
    CartaMazo,
    Favorito,
    Comparar,
    Simbolo
}