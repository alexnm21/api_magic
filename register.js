const express = require('express'),
    router = express.Router(),
    respuestaError = require("./model/respuestaError"),
    { Usuario } = require('./model/dbSequelize'),
    Bcrypt = require('./global_functions/Bcrypt');


router.post("/", async (req, res) => {

    if (comprobarCamposRellenos(req.body, res)) {
        const usuarioUnico = await comprobarUniqueUsuario(req.body, res);

        if(usuarioUnico.count === 0){
            insertarDatos(req.body, res);
        }else{
            res.status(400).json(new respuestaError(-4, "Usuario ya existente"));
        }
    }

});

function comprobarCamposRellenos(datos, res) {

    if (datos.nombre === undefined || datos.apellidos === undefined || datos.usuario === undefined || datos.password === undefined) {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    if (datos.nombre.trim() === "" || datos.apellidos.trim() === "" || datos.usuario.trim() === "" || datos.password.trim() === "") {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    return true;
}

function comprobarUniqueUsuario(datos, res) {

    return Usuario.findAndCountAll({
        where: {
            usuario: datos.usuario
        }
    }).then(result => {
        return result;
    }).catch(error => {
        console.log(error);
    })

}

async function insertarDatos(datos, res) {

    Usuario.create({
        nombre: datos.nombre,
        apellidos: datos.apellidos,
        usuario: datos.usuario,
        password: await Bcrypt.encriptarPassword(datos.password),
        img_perfil: datos.img_perfil
    }).then(result => {
        delete result.dataValues.password;
        res.json({message: "Usuario agregado correctamente", data: result});
    }).catch(error => {
        console.log(error);
    });

}

module.exports = router;