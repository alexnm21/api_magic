const download = require('download');
const fs = require('fs');
const { Card } = require('./model/dbSequelize');
const fetch = require('node-fetch');
const moment = require('moment');

const JSONA = '/CardsMagicEN.json';
const JSONB = '/CardsMagicEN_new.json';
const FILE_TIME_UPDATE = "/tiempoDeActualizacion.json";

function updatebbdd() {
    checkIfData()
        .then(result => {
            if (result.count === 0) {
                downloadJson('first')
                    .then(result => {
                        let jsonA = leerFichero(__dirname + JSONA);
                        let cartas = introduceDatos(jsonA);

                        insertaCartas(cartas);
                    })
                    .catch(err => {
                       console.log(err);
                    });
            } else {
                if (fs.existsSync(__dirname + FILE_TIME_UPDATE)) {
                    let [object_time] = leerFichero(__dirname + FILE_TIME_UPDATE);
                    let time_update = montarFecha(object_time);

                    if (moment(time_update, "D/M/YYYY HH:mm").isBefore()) {
                        //La fecha ya ha pasado, entonces actualiza
                        actualizaTiempoDeActualizacion(object_time);
                        downloadJson('second')
                            .then(result => {
                                comparaJsons();
                            })
                            .catch(err => {
                                console.log(err);
                            })
                    }

                }
            }
        })
        .catch(err => {
            console.log(err);
        })

}

function actualizaTiempoDeActualizacion(object_time) {
    let new_date = moment().add(24, 'hours').format("D/M/YYYY HH:mm");
    object_time.fecha_actualizacion = new_date;

    let newArray = [object_time];
    let json = JSON.stringify(newArray);

    fs.writeFileSync(__dirname + FILE_TIME_UPDATE, json, 'utf8');
}

function montarFecha(object_time) {
    let day = moment(object_time.fecha_actualizacion, "D/M/YYYY HH:mm").get('date');
    let month = moment(object_time.fecha_actualizacion, "D/M/YYYY HH:mm").get('month');
    let year = moment(object_time.fecha_actualizacion, "D/M/YYYY HH:mm").get('year');
    let hour = moment(object_time.fecha_actualizacion, "D/M/YYYY HH:mm").get('hour');
    let minute = moment(object_time.fecha_actualizacion, "D/M/YYYY HH:mm").get('minute');

    return time_update = moment().set({ 'date': day, 'month': month, 'year': year, 'hour': hour, 'minute': minute });
}

function leerFichero(fichero) {
    let rawData = fs.readFileSync(fichero);
    return JSON.parse(rawData);
}

async function downloadJson(type) {

    let url = "https://api.scryfall.com/bulk-data";
    const bulkUrlData = await fetch(url);
    const bulkDataInfo = await bulkUrlData.json();
    const cardsUrl = bulkDataInfo.data[2].download_uri;

    if (type === "first") {
        // Descarga el primer Json (JsonA)

        //DESCARGA EL ARCHIVO Y LO GUARDA, DESPUES DE DESCARGARLO REINICIA EL SERVIDOR
        //SI YA HAY UN ARCHIVO QUE SE LLAMA IGUAL LO SOBREESCRIBE, VA PERFECTO
        //DESPUES DEL DOWNLOAD NO SE EJECUTA NADA MÁS
        //POR ESO LO HE PUESTO EN EL GET Y NO TODO JUNTO EN EL POST
        return await download(cardsUrl, __dirname, { filename: JSONA });

    } else {
        // Descarga el nuevo Json (JsonB)
        return await download(cardsUrl, __dirname, { filename: JSONB })
    }

}

function comparaJsons() {
    let dataA = fs.readFileSync(__dirname + JSONA);
    let dataB = fs.readFileSync(__dirname + JSONB);
    let jsonA = JSON.parse(dataA);
    let jsonB = JSON.parse(dataB);

    let cartasNuevas = [];
    let cartasCambiadas = [];
    let modificado = false;

    //Compruebo si se han añadido mas cartas
    if (jsonA.length !== jsonB.length) {
        let indice_ultima_carta_jsonA = jsonA.indexOf(jsonA[jsonA.length - 1]);
        let indice_ultima_carta_jsonB = jsonB.indexOf(jsonB[jsonB.length - 1]);

        for (i = indice_ultima_carta_jsonA + 1; i <= indice_ultima_carta_jsonB; i++) {
            cartasNuevas.push(jsonB[i]);
        }

        if (cartasNuevas.length > 0) {
            let cartas = introduceDatos(cartasNuevas);
            insertaCartas(cartas);
            modificado = true;
        }

    }

    jsonA.forEach((carta, index) => {
        if (JSON.stringify(carta) !== JSON.stringify(jsonB[index])) {
            cartasCambiadas.push(jsonB[index]);
        }
    })

    if (cartasCambiadas.length > 0) {
        let cartas = introduceDatos(cartasCambiadas);
        actualizaCartas(cartas);
        modificado = true;
    }

    if (modificado) {
        borrarJsonA();
        cambiarArchivoJsonA();
    }
}

function insertaCartas(cartas) {
    cartas.forEach(carta => {
        Card.create(carta)
            .catch(err => {
                console.log(err);
            })
    })
}

function actualizaCartas(cartas) {
    cartas.forEach(carta => {
        Card.update(carta, {
            where: {
                id: carta.id
            }
        })
            .then(result => {
                console.log(result)
            })
            .catch(err => {
                console.log(error);
            })
    })
}

function borrarJsonA() {
    fs.unlink(__dirname + JSONA, (err) => {
        if (err) throw err;
    });
}

function cambiarArchivoJsonA() {
    fs.rename(__dirname + JSONB, __dirname + JSONA, (err) => {
        if (err) throw err;
        console.log('File changed!');
    });
}

async function checkIfData() {
    return await Card.findAndCountAll();
}

function introduceDatos(cartas) {
    let values = [];

    cartas.forEach(carta => {
        let layout = carta.layout;

        switch (layout) {
            case "normal":
                values.push(normal(carta));
                break;
            case "split":
                values.push(split(carta));
                break;
            case "flip":
                values.push(flip(carta));
                break;
            case "transform":
                values.push(transform(carta));
                break;
            case "modal_dfc":
                values.push(modal_dfc(carta));
                break;
            case "meld":
                values.push(meld(carta));
                break;
            case "leveler":
                values.push(leveler(carta));
                break;
            case "saga":
                values.push(saga(carta));
                break;
            case "adventure":
                values.push(adventure(carta));
                break;
            case "planar":
                values.push(planar(carta));
                break;
            case "scheme":
                values.push(scheme(carta));
                break;
            case "vanguard":
                values.push(vanguard(carta));
                break;
            case "token":
                values.push(token(carta));
                break;
            case "double_faced_token":
                values.push(double_faced_token(carta));
                break;
            case "emblem":
                values.push(emblem(carta));
                break;
            case "augment":
                values.push(augment(carta));
                break;
            case "host":
                values.push(host(carta));
                break;
            case "art_series":
                values.push(art_series(carta));
                break;
            case "double_sided":
                values.push(double_sided(carta));
                break;
            default: console.log("No existe el layout definido");
        }

    })

    return values;

}

function normal(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.name,
        name_back: null,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.mana_cost,
        mana_cost_back: null,
        cmc: carta.cmc,
        power_front: carta.power,
        power_back: null,
        toughness_front: carta.toughness,
        toughness_back: null,
        typeline_front: carta.type_line,
        typeline_back: null,
        descripcion_front: carta.oracle_text,
        descripcion_back: null,
        flavor_text_front: carta.flavor_text,
        flavor_text_back: null,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function split(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.card_faces[0].name,
        name_back: carta.card_faces[1].name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.card_faces[0].mana_cost,
        mana_cost_back: carta.card_faces[1].mana_cost,
        cmc: carta.cmc,
        power_front: carta.card_faces[0].power,
        power_back: carta.card_faces[1].power,
        toughness_front: carta.card_faces[0].toughness,
        toughness_back: carta.card_faces[1].toughness,
        typeline_front: carta.card_faces[0].type_line,
        typeline_back: carta.card_faces[1].type_line,
        descripcion_front: carta.card_faces[0].oracle_text,
        descripcion_back: carta.card_faces[1].oracle_text,
        flavor_text_front: carta.card_faces[0].flavor_text,
        flavor_text_back: carta.card_faces[1].flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function flip(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.card_faces[0].name,
        name_back: carta.card_faces[1].name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.card_faces[0].mana_cost,
        mana_cost_back: carta.card_faces[1].mana_cost,
        cmc: carta.cmc,
        power_front: carta.card_faces[0].power,
        power_back: carta.card_faces[1].power,
        toughness_front: carta.card_faces[0].toughness,
        toughness_back: carta.card_faces[1].toughness,
        typeline_front: carta.card_faces[0].type_line,
        typeline_back: carta.card_faces[1].type_line,
        descripcion_front: carta.card_faces[0].oracle_text,
        descripcion_back: carta.card_faces[1].oracle_text,
        flavor_text_front: carta.card_faces[0].flavor_text,
        flavor_text_back: carta.card_faces[1].flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function transform(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.card_faces[0].name,
        name_back: carta.card_faces[1].name,
        img_small: carta.card_faces[0].image_uris.small,
        img_normal: carta.card_faces[0].image_uris.normal,
        img_large: carta.card_faces[0].image_uris.large,
        // img_small_back: carta.card_faces[1].image_uris.small,
        // img_normal_back: carta.card_faces[1].image_uris.normal,
        // img_large_back: carta.card_faces[1].image_uris.large,
        mana_cost_front: carta.card_faces[0].mana_cost,
        mana_cost_back: carta.card_faces[1].mana_cost,
        cmc: carta.cmc,
        power_front: carta.card_faces[0].power,
        power_back: carta.card_faces[1].power,
        toughness_front: carta.card_faces[0].toughness,
        toughness_back: carta.card_faces[1].toughness,
        typeline_front: carta.card_faces[0].type_line,
        typeline_back: carta.card_faces[1].type_line,
        descripcion_front: carta.card_faces[0].oracle_text,
        descripcion_back: carta.card_faces[1].oracle_text,
        flavor_text_front: carta.card_faces[0].flavor_text,
        flavor_text_back: carta.card_faces[1].flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function modal_dfc(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.card_faces[0].name,
        name_back: carta.card_faces[1].name,
        img_small: carta.card_faces[0].image_uris.small,
        img_normal: carta.card_faces[0].image_uris.normal,
        img_large: carta.card_faces[0].image_uris.large,
        // img_small_back: carta.card_faces[1].image_uris.small,
        // img_normal_back: carta.card_faces[1].image_uris.normal,
        // img_large_back: carta.card_faces[1].image_uris.large,
        mana_cost_front: carta.card_faces[0].mana_cost,
        mana_cost_back: carta.card_faces[1].mana_cost,
        cmc: carta.cmc,
        power_front: carta.card_faces[0].power,
        power_back: carta.card_faces[1].power,
        toughness_front: carta.card_faces[0].toughness,
        toughness_back: carta.card_faces[1].toughness,
        typeline_front: carta.card_faces[0].type_line,
        typeline_back: carta.card_faces[1].type_line,
        descripcion_front: carta.card_faces[0].oracle_text,
        descripcion_back: carta.card_faces[1].oracle_text,
        flavor_text_front: carta.card_faces[0].flavor_text,
        flavor_text_back: carta.card_faces[1].flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function meld(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.mana_cost,
        cmc: carta.cmc,
        power_front: carta.power,
        toughness_front: carta.toughness,
        typeline_front: carta.type_line,
        descripcion_front: carta.oracle_text,
        flavor_text_front: carta.flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function leveler(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.mana_cost,
        cmc: carta.cmc,
        power_front: carta.power,
        toughness_front: carta.toughness,
        typeline_front: carta.type_line,
        descripcion_front: carta.oracle_text,
        flavor_text_front: carta.flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function saga(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.mana_cost,
        cmc: carta.cmc,
        power_front: carta.power,
        toughness_front: carta.toughness,
        typeline_front: carta.type_line,
        descripcion_front: carta.oracle_text,
        flavor_text_front: carta.flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function adventure(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.card_faces[0].name,
        name_back: carta.card_faces[1].name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.card_faces[0].mana_cost,
        mana_cost_back: carta.card_faces[1].mana_cost,
        cmc: carta.cmc,
        power_front: carta.card_faces[0].power,
        power_back: carta.card_faces[1].power,
        toughness_front: carta.card_faces[0].toughness,
        toughness_back: carta.card_faces[1].toughness,
        typeline_front: carta.card_faces[0].type_line,
        typeline_back: carta.card_faces[1].type_line,
        descripcion_front: carta.card_faces[0].oracle_text,
        descripcion_back: carta.card_faces[1].oracle_text,
        flavor_text_front: carta.card_faces[0].flavor_text,
        flavor_text_back: carta.card_faces[1].flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function planar(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.mana_cost,
        cmc: carta.cmc,
        power_front: carta.power,
        toughness_front: carta.toughness,
        typeline_front: carta.type_line,
        descripcion_front: carta.oracle_text,
        flavor_text_front: carta.flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function scheme(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.mana_cost,
        cmc: carta.cmc,
        power_front: carta.power,
        toughness_front: carta.toughness,
        typeline_front: carta.type_line,
        descripcion_front: carta.oracle_text,
        flavor_text_front: carta.flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function vanguard(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.mana_cost,
        cmc: carta.cmc,
        power_front: carta.power,
        toughness_front: carta.toughness,
        typeline_front: carta.type_line,
        descripcion_front: carta.oracle_text,
        flavor_text_front: carta.flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function token(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.mana_cost,
        cmc: carta.cmc,
        power_front: carta.power,
        toughness_front: carta.toughness,
        typeline_front: carta.type_line,
        descripcion_front: carta.oracle_text,
        flavor_text_front: carta.flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function double_faced_token(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.card_faces[0].name,
        name_back: carta.card_faces[1].name,
        img_small: carta.card_faces[0].image_uris.small,
        img_normal: carta.card_faces[0].image_uris.normal,
        img_large: carta.card_faces[0].image_uris.large,
        // img_small_back: carta.card_faces[1].image_uris.small,
        // img_normal_back: carta.card_faces[1].image_uris.normal,
        // img_large_back: carta.card_faces[1].image_uris.large,
        mana_cost_front: carta.card_faces[0].mana_cost,
        mana_cost_back: carta.card_faces[1].mana_cost,
        cmc: carta.cmc,
        power_front: carta.card_faces[0].power,
        power_back: carta.card_faces[1].power,
        toughness_front: carta.card_faces[0].toughness,
        toughness_back: carta.card_faces[1].toughness,
        typeline_front: carta.card_faces[0].type_line,
        typeline_back: carta.card_faces[1].type_line,
        descripcion_front: carta.card_faces[0].oracle_text,
        descripcion_back: carta.card_faces[1].oracle_text,
        flavor_text_front: carta.card_faces[0].flavor_text,
        flavor_text_back: carta.card_faces[1].flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function emblem(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.mana_cost,
        cmc: carta.cmc,
        power_front: carta.power,
        toughness_front: carta.toughness,
        typeline_front: carta.type_line,
        descripcion_front: carta.oracle_text,
        flavor_text_front: carta.flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set
    }
}

function augment(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.mana_cost,
        cmc: carta.cmc,
        power_front: carta.power,
        toughness_front: carta.toughness,
        typeline_front: carta.type_line,
        descripcion_front: carta.oracle_text,
        flavor_text_front: carta.flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set,
    }
}

function host(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.name,
        img_small: carta.image_uris.small,
        img_normal: carta.image_uris.normal,
        img_large: carta.image_uris.large,
        mana_cost_front: carta.mana_cost,
        cmc: carta.cmc,
        power_front: carta.power,
        toughness_front: carta.toughness,
        typeline_front: carta.type_line,
        descripcion_front: carta.oracle_text,
        flavor_text_front: carta.flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set,
    }
}

function art_series(carta) {
    return {
        id: carta.id,
        fecha_lanzamiento: carta.released_at,
        name_front: carta.card_faces[0].name,
        name_back: carta.card_faces[1].name,
        img_small: carta.card_faces[0].image_uris.small,
        img_normal: carta.card_faces[0].image_uris.normal,
        img_large: carta.card_faces[0].image_uris.large,
        // img_small_back: carta.card_faces[1].image_uris.small,
        // img_normal_back: carta.card_faces[1].image_uris.normal,
        // img_large_back: carta.card_faces[1].image_uris.large,
        mana_cost_front: carta.card_faces[0].mana_cost,
        mana_cost_back: carta.card_faces[1].mana_cost,
        cmc: carta.cmc,
        power_front: carta.card_faces[0].power,
        power_back: carta.card_faces[1].power,
        toughness_front: carta.card_faces[0].toughness,
        toughness_back: carta.card_faces[1].toughness,
        typeline_front: carta.card_faces[0].type_line,
        typeline_back: carta.card_faces[1].type_line,
        descripcion_front: carta.card_faces[0].oracle_text,
        descripcion_back: carta.card_faces[0].oracle_text,
        flavor_text_front: carta.card_faces[0].flavor_text,
        flavor_text_back: carta.card_faces[0].flavor_text,
        layout: carta.layout,
        rarity: carta.rarity,
        precio_eur: carta.prices.eur,
        code_set: carta.set,
    }
}

//NO HAY NINGUNA CARTA DE ESTE TIPO PERO IGUAL HAY QUE ACTIVARLO
function double_sided(carta) {
    return {
        // id: carta.id,
        // fecha_lanzamiento: carta.released_at,
        // name_front: carta.name,
        // // name_back: carta.card_faces[1].name,
        // img_small: carta.image_uris.small,
        // img_normal: carta.image_uris.normal,
        // img_large: carta.image_uris.large,
        // // img_small_back: carta.card_faces[1].image_uris.small,
        // // img_normal_back: carta.card_faces[1].image_uris.normal,
        // // img_large_back: carta.card_faces[1].image_uris.large,
        // mana_cost_front: carta.mana_cost,
        // // mana_cost_back: carta.card_faces[1].mana_cost,
        // cmc: carta.cmc,
        // power_front: carta.power,
        // // power_back: carta.card_faces[1].power,
        // toughness_front: carta.toughness,
        // // toughness_back: carta.card_faces[1].toughness,
        // typeline_front: carta.type_line,
        // // typeline_back: carta.card_faces[1].type_line,
        // descripcion_front: carta.oracle_text,
        // // descripcion_back: carta.card_faces[0].oracle_text,
        // flavor_text_front: carta.flavor_text,
        // // flavor_text_back: carta.card_faces[0].flavor_text,
        // layout: carta.layout,
        // rarity: carta.rarity,
        // precio_eur: carta.prices.eur,
        // code_set: carta.set
    }
}


module.exports = { updatebbdd }