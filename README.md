API realizada por Alex Nieto Mendez y Oda Solà Lopez.  
Linkedin Alex: https://www.linkedin.com/in/alexnietomendez/  
Linkedin Oda: https://www.linkedin.com/in/odasolalopez/  

DOCUMENTACIÓN DEL PROYECTO: https://docs.google.com/document/d/1BzwJUBkx8pQyggiyZuczWQm7El2Chccls0miwsQE8Cg/edit?usp=sharing

## Importar SQL
Importa el sql llamado "magic.sql.zip" a tu base de datos local, puedes hacerlo mediante phpmyadmin.

Nueva base de datos>Importar>Seleccionar archivo (la importación tardará unos minutos).

## Instalar dependencias
Situate mediante el terminal de comandos en el directorio del proyecto e instale las dependencias mediante el comando "npm i".

## Configuración
Crea un archivo llamado ".env" y añade las siguientes lineas:

DB_NAME=magic  
DB_USER=root  
DB_HOST=localhost  
DB_PASS=  

(Donde DB_USER y DB_PASS tendrás que poner el usuario y contraseña de tu base de datos local)

## Iniciar
Escribe en el terminal estando situado en el directorio del proyecto "npm start"

## Usuario por defecto
Usuario: usuario1  
Contraseña: usu1  

## Actualizar datos (opcional)
Si quieres que se actualicen los datos de la API, ves al archivo "app.js" y descomenta la linea 41, funcion "updatebbdd()". 
Si descomentas esta linea la base de datos se actualizará cada 24 horas (la actualizacion puede tardar hasta 30 minutos si lleva mucho tiempo sin actualizarse).




