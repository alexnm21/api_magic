const bcrypt = require('bcrypt');
const BCRYPT_SALT_ROUNDS = 10;

class Bcrypt {

    static encriptarPassword(password) {
        return bcrypt.hash(password, BCRYPT_SALT_ROUNDS)
            .then((hashedPassword) => {
                return hashedPassword;
            })
            .catch(err => {
                console.log(err);
            })

    }

    static comparePasswords(password_user, password_bbdd){
        return bcrypt.compare(password_user, password_bbdd)
            .then((result) => {
                return result;
            })
            .catch(err => {
                console.log(err);
            })
    }
}

module.exports = Bcrypt;