const Bcrypt = require('./global_functions/Bcrypt');

const express = require('express'),
    router = express.Router(),
    respuestaError = require("./model/respuestaError"),
    jwt = require("jsonwebtoken"),
    config = require("./configs/config"),
    { Usuario } = require('./model/dbSequelize');

router.post("/", (req, res) => {
    let datos = req.body;

    if (validarDatos(datos)) {

        Usuario.findAndCountAll({
            where: {
                usuario: datos.usuario
            }
        }).then(async (result) => {
            if (result.count > 0) {
                let password_bbdd = result.rows[0].password;
                let samePassword = await Bcrypt.comparePasswords(datos.password, password_bbdd);

                if (samePassword) {
                    const payload = {
                        check: true
                    };

                    const token = jwt.sign(payload, config.key_token);
                    insertarTokenBBDD(result.rows[0].id, token);

                    delete result.rows[0].dataValues.password;
                    res.json({message:"Usuario logueado correctamente", data: result.rows[0]});
                }else{
                    res.status(401).json(new respuestaError(-9, "Contraseña no existente"));
                }


            } else {
                res.status(401).json(new respuestaError(-9, "Usuario no existente"));
            }

        }).catch(error => {
            console.log(error);
        })

    } else {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
    }

})

function validarDatos(datos) {
    let valid = true;

    if (datos.usuario === undefined || datos.password === undefined) {
        valid = false;
    } else if (datos.usuario.trim() === "" || datos.password.trim() === "") {
        valid = false
    }

    return valid;
}

function insertarTokenBBDD(id_usu, token) {
    Usuario.update({
        token: token
    }, {
        where: {
            id: id_usu
        }
    }).catch(error => {
        console.log(error);
    })
}

module.exports = router;