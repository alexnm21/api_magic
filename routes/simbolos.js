const router = require('express').Router();
const { Simbolo } = require('../model/dbSequelize');
const respuestaError = require('../model/respuestaError');

//-- MUESTRA TODOS LOS SIMBOLOS --
router.get('/', async (req, res) => {

    Simbolo.findAll()
        .then(result => {
            res.json(result);
        })
        .catch(err => {
            console.log(err);
        })

});

//-- MUESTRA EL RESULTADO DADO SU SIMBOLO --
router.get('/:symbol', async (req, res) => {

    Simbolo.findOne({
        where: {
            simbolo: req.params.symbol
        }
    })
        .then(result => {
            if (result !== null) res.json(result);
            else res.status(400).json(new respuestaError(-5, "El simbolo pasado no existe"));
        })
        .catch(err => {
            console.log(err);
        })

});

//-- MUESTRA LOS RESULTADOS DADOS VARIOS SIMBOLOS --
router.post('/', async (req, res) => {

    let simbolos = req.body.symbols;

    if (Array.isArray(simbolos)) {
        Simbolo.findAll({
            where: {
                simbolo: req.body.symbols
            }
        })
            .then(result => {
                res.json(result);
            })
            .catch(err => {
                console.log(err);
            })
    }else{
        res.status(400).json(new respuestaError(-8, "El valor que has pasado no es un Array"));
    }



});

module.exports = router;