const router = require('express').Router();
const { Mazo, Usuario, CartaMazo, Card } = require('../model/dbSequelize');
const respuestaError = require('../model/respuestaError');


//-- CHECKEA QUE USUARIO ESTA HACIENDO LA PETICION --
function currentUser(req, res) {
    const userActual = Usuario.findOne({
        where: {
            token: req.headers['access-token']
        }
    }).then(user => {
        return user;
    }).catch(err => {
        console.log(err);
        res.status(401).json(new respuestaError(-6, "Sesión expirada"));
    });

    return userActual;
}

//-- MUESTRA TODOS LOS MAZOS DEL USUARIO --
router.get('/', async (req, res) => {

    const userActual = await currentUser(req, res);

    if (userActual !== null) {
        Mazo.findAll({
            where: {
                id_usuario: userActual.id
            }
        })
        .then((result) => {
            res.json(result);
        })
        .catch(err => {
            console.log(err);
        })
        
    } else {
        res.status(400).json(new respuestaError(-2, "No existe el token"));
    }

});

//-- MUESTRA TODAS LAS CARTAS QUE HAY DENTRO DE UN MAZO --
router.get('/:mazoId', async (req, res) => {

    const userActual = await currentUser(req, res);

    if (userActual !== null) {
        const mazoActual = await findMazo(req.params.mazoId, userActual.id);

        Card.hasMany(CartaMazo, { foreignKey: 'id_carta' });
        CartaMazo.belongsTo(Card, { foreignKey: 'id_carta' });

        if (mazoActual !== null) {
            CartaMazo.findAll({
                where: {
                    id_mazo: req.params.mazoId
                },
                include: [{
                    model: Card
                }]
            })
                .then(result => {
                    if (result.length > 0) {
                        let respuesta = result.map(el => ({ "id_carta_mazo": el.id, "carta": el.carta }));
                        res.json(respuesta);
                    } else {
                        res.json(result);
                    }
                })
                .catch(err => {
                    console.log(err);
                })
        } else {
            res.status(400).json(new respuestaError(-5, "Este mazo no existe"));
        }
    } else {
        res.status(400).json(new respuestaError(-2, "No existe el token"));
    }

});

//-- CREA UN MAZO --
router.post('/', async (req, res) => {
    if (comprobarCamposRellenosMazos(req.body, res)) {
        const userActual = await currentUser(req, res);

        if (userActual !== null) {

            Mazo.create({
                nombre: req.body.nombre,
                id_usuario: userActual.id
            }).then(result => {
                res.json({ message: "Mazo creado", data: result });
            }).catch(err => {
                console.log(err);
            });

        } else {
            res.status(400).json(new respuestaError(-2, "No existe el token"));
        }
    }


});

//-- AÑADE UNA CARTA POR ID AL MAZO CORRESPONDIENTE --
router.post('/:mazoId/addCard', async (req, res) => {

    if (comprobarCamposRellenosCartasMazos(req.body, res)) {
        const userActual = await currentUser(req, res);

        if (userActual !== null) {
            const mazoActual = await findMazo(req.params.mazoId, userActual.id);

            const cartaAgregar = await Card.findOne({
                where: {
                    id: req.body.idCard,
                }
            }).catch(err => {
                console.log(err);
            });

            if (mazoActual !== null && cartaAgregar !== null) {
                CartaMazo.create({
                    id_mazo: req.params.mazoId,
                    id_carta: req.body.idCard
                }).then(result => {
                    res.json({ message: "Carta añadida al mazo", data: result });
                }).catch(err => {
                    console.log(err);
                });

            } else {
                res.status(400).json(new respuestaError(-5, "No se ha encontrado el mazo o la carta correspondiente"));
            }
        } else {
            res.status(400).json(new respuestaError(-2, "No existe el token"));
        }
    }

});


//-- MODIFICA LOS DATOS DE UN MAZO --
router.put('/:mazoId', async (req, res) => {

    if (comprobarCamposRellenosMazos(req.body, res)) {
        const userActual = await currentUser(req, res);

        if (userActual !== null) {
            Mazo.update(
                { nombre: req.body.nombre },
                {
                    where: {
                        id: req.params.mazoId,
                        id_usuario: userActual.id
                    }
                }).then(() => {
                    res.json({message: "Mazo actualizado"});
                }).catch(err => {
                    console.log(err);
                });
        } else {
            res.status(400).json(new respuestaError(-2, "No existe el token"));
        }
    }

});

//-- ELIMINA UN MAZO --
router.delete('/:mazoId', async (req, res) => {

    const userActual = await currentUser(req, res);

    if (userActual !== null) {
        const mazo = await findMazo(req.params.mazoId, userActual.id);

        if (mazo !== null) {
            const existenCartas = await CartaMazo.findOne({ where: { id_mazo: req.params.mazoId } })
                .catch(err => {
                    console.log(err);
                })

            if (existenCartas === null) {
                Mazo.destroy({
                    where: {
                        id: req.params.mazoId,
                        id_usuario: userActual.id
                    }
                }).then(() => {
                    res.json({message: "Mazo eliminado"});
                }).catch(err => {
                    console.log(err);
                });
            } else {
                res.json(new respuestaError(-7, "Existen cartas en este mazo"));
            }
        } else {
            res.status(400).json(new respuestaError(-5, "El mazo no existe"));
        }
    } else {
        res.status(400).json(new respuestaError(-2, "No existe el token"));
    }

})


//-- ELIMINA UNA CARTA DE UN MAZO --
router.delete('/:mazoId/deleteCard/:idCartaMazo', async (req, res) => {

    const userActual = await currentUser(req, res);

    if (userActual !== null) {
        const mazoActual = await findMazo(req.params.mazoId, userActual.id);
        const carta = await findCartaMazo(req.params.idCartaMazo, req.params.mazoId);

        if (mazoActual !== null && carta !== null) {

            CartaMazo.destroy({
                where: {
                    id: req.params.idCartaMazo,
                    id_mazo: req.params.mazoId
                }
            }).then(() => {
                res.json({ message: "Carta eliminada del mazo"});
            }).catch(err => {
                console.log(err);
            })

        } else {
            res.status(400).json(new respuestaError(-5, "La carta no existe"));
        }
    } else {
        res.status(400).json(new respuestaError(-2, "No existe el token"));
    }

});

//-- ELIMINA TODAS LAS CARTAS DE UN MAZO --
router.delete('/:mazoId/deleteAllCards', async (req, res) => {

    const userActual = await currentUser(req, res);

    if (userActual !== null) {
        const mazoActual = await findMazo(req.params.mazoId, userActual.id);

        if (mazoActual !== null) {

            CartaMazo.destroy({
                where: {
                    id_mazo: req.params.mazoId
                }
            }).then((result) => {
                if (result > 0) res.json({message: "Todas las cartas del mazo han sido eliminadas"});
                else res.json({message: "No se ha eliminado ninguna carta"});     
            }).catch(err => {
                console.log(err);
            })

        } else {
            res.status(400).json(new respuestaError(-5, "El mazo no existe"));
        }
    } else {
        res.status(400).json(new respuestaError(-2, "No existe el token"));
    }

});


function comprobarCamposRellenosMazos(datos, res) {

    if (datos.nombre === undefined) {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    if (datos.nombre.trim() === "") {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    return true;
}

function comprobarCamposRellenosCartasMazos(datos, res) {

    if (datos.idCard === undefined) {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    if (datos.idCard.trim() === "") {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    return true;
}

function findMazo(id_mazo, id_usuario) {
    return Mazo.findOne({
        where: {
            id: id_mazo,
            id_usuario: id_usuario
        }
    }).catch(err => {
        console.log(err);
    });
}

function findCartaMazo(id_carta_mazo, id_mazo) {
    return CartaMazo.findOne({
        where: {
            id: id_carta_mazo,
            id_mazo: id_mazo
        }
    }).catch(err => {
        console.log(err);
    });
}

module.exports = router;
