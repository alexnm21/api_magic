const router = require('express').Router();
const { Usuario } = require('../model/dbSequelize');
const respuestaError = require('../model/respuestaError');

function currentUser(req, res) {
    const userActual = Usuario.findOne({
        where: {
            token: req.headers['access-token']
        }
    }).then(user => {
        if(user===null) res.json(new respuestaError(-2, "No existe el token"))
        else return user;
    }).catch(err => {
        console.log(err);
        res.status(400).json(new respuestaError(-6, 'Sesión expirada'));
    });

    return userActual;
}

router.get('/', async (req, res) => {
    const userActual = await currentUser(req, res);

    Usuario.update(
        { token: null }, {
        where: {
            id: userActual.id
        }
    })
    .then(() => {
        res. json({message: "Deslogueo con exito"});
    })
    .catch(err => {
        console.log(err);
    })

});

module.exports = router;