const router = require('express').Router();
const { Set } = require('../model/dbSequelize');
const respuestaError = require('../model/respuestaError');

//DEVUELVE TODOS LOS SETS
router.get('/', async (req, res) => {
    const set = await Set.findAll();
    res.json(set);
});

//DEVUELVE EL SET PASADO SU CODE
router.get('/:setCode', (req, res) => {
    Set.findOne({ where: { code: req.params.setCode } })
        .then(respuesta => {
            if(respuesta!==null){
                res.json(respuesta);
            }else{
                res.status(400).json(new respuestaError(-5, "No se ha encontrado un set con el code pasado"));
            }
            
        }).catch(err => {
            console.log(err);
        });
});


module.exports = router;