const router = require('express').Router();
const { Card, Favorito, Usuario } = require('../model/dbSequelize');
const respuestaError = require('../model/respuestaError');

//-- CHECKEA QUE USUARIO ESTA HACIENDO LA PETICION --
function currentUser(req, res) {
    const userActual = Usuario.findOne({
        where: {
            token: req.headers['access-token']
        }
    }).then(user => {
        return user;
    }).catch(err => {
        console.log(err);
        res.status(401).json(new respuestaError(-6, "Sesión expirada"));
    });

    return userActual;
}

//-- LEE LAS CARTAS QUE HAY EN FAVORITOS --
router.get('/', async (req, res) => {

    const userActual = await currentUser(req, res);

    if (userActual !== null) {
        Card.hasMany(Favorito, { foreignKey: 'id_carta' });
        Favorito.belongsTo(Card, { foreignKey: 'id_carta' });

        Favorito.findAll({
            where: {
                id_usuario: userActual.id
            },
            include: [{
                model: Card
            }]
        })
            .then(result => {
                if (result.length > 0) {
                    let cartas = result.map(card => card.carta)
                    res.json(cartas);
                } else {
                    res.json(result);
                }
            })
            .catch(err => {
                console.log(err);
            })
    } else {
        res.status(400).json(new respuestaError(-2, "No existe el token"));
    }

});


//-- AÑADE UNA CARTA A FAVORITOS --
router.post('/', async (req, res) => {

    if (comprobarCamposRellenosFavoritos(req.body, res)) {
        const userActual = await currentUser(req, res);

        if (userActual !== null) {
            const existeCarta = await findCardByIdCardInCards(req.body.idCard);
            
            if (existeCarta !== null) {
                
                const cartaExisteEnFavoritos = await comprobarSiCartaYaExisteEnFavoritos(req.body.idCard, userActual.id);
                
                if (cartaExisteEnFavoritos === null) {
                    Favorito.create({
                        id_carta: req.body.idCard,
                        id_usuario: userActual.id
                    }).then(results => {
                        res.json({ message: "Carta añadida a favoritos", data: results });
                    }).catch(err => {
                        console.log(err);
                    });
                } else {
                    res.status(400).json(new respuestaError(-4, "Esta carta ya esta añadida"));
                }
            } else {
                res.status(400).json(new respuestaError(-5, "Esta carta no existe"));
            }
        } else {
            res.status(400).json(new respuestaError(-2, "No existe el token"));
        }
    }

});

//-- BORRA UNA CARTA DE TUS FAVORITOS --
router.delete('/', async (req, res) => {

    if (comprobarCamposRellenosFavoritos(req.body, res)) {
        const userActual = await currentUser(req, res);

        if (userActual !== null) {
            const existeCarta = await findCardByIdCardInFavorites(req.body.idCard, userActual.id);

            if (existeCarta !== null) {
                Favorito.destroy({
                    where: {
                        id_carta: req.body.idCard,
                        id_usuario: userActual.id
                    }
                }).then(() => {
                    res.json({ message: "La carta se ha borrado con exito"});
                })
                .catch(err => {
                    console.log(err);
                });

            } else {
                res.status(400).json(new respuestaError(-4, "La carta que intentas borrar no existe"));
            }
        } else {
            res.status(400).json(new respuestaError(-2, "No existe el token"));
        }
    }

});

function findCardByIdCardInFavorites(id_card, id_usuario) {
    return Favorito.findOne({
        where: {
            id_carta: id_card,
            id_usuario: id_usuario
        }
    }).catch(err => {
        console.log(err);
    });
}

function findCardByIdCardInCards(id_card) {
    return Card.findOne({
        where: {
            id: id_card
        }
    }).catch(err => {
        console.log(err);
    });
}

function comprobarSiCartaYaExisteEnFavoritos(id_card, id_usuario) {
    return Favorito.findOne({
        where: {
            id_carta: id_card,
            id_usuario: id_usuario
        }
    }).catch(err => {
        console.log(err);
    });
}

function comprobarCamposRellenosFavoritos(datos, res) {

    if (datos.idCard === undefined) {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    if (datos.idCard.trim() === "") {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    return true;
}

module.exports = router;