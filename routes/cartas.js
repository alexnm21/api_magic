const router = require('express').Router();
const { Card } = require('../model/dbSequelize');
const respuestaError = require('../model/respuestaError');

//DEVUELVE LA CARTA PASADA SU ID
router.get('/:cardId', (req, res) => {
    Card.findOne({ where: { id: req.params.cardId } })
        .then(respuesta => {
            respuesta!==null ? res.json(respuesta) : res.status(400).json(new respuestaError(-5, "No se ha encontrado la carta con el id pasado"));
        }).catch(err => {
            console.log(err);
        });
});

//DEVUELVE LAS CARTAS PERTNECIENTES AL SET PASADO SU CODE
router.get('/set/:setCode', (req, res) => {
    Card.findAll({ where: { code_set: req.params.setCode } })
        .then(respuesta => {
            res.json(respuesta);
        }).catch(err => {
            console.log(err);
        });
});

module.exports = router;
