const express = require('express'),
    router = express.Router(),
    respuestaError = require("../model/respuestaError"),
    { Usuario } = require('../model/dbSequelize'),
    Sequelize = require('sequelize'),
    Op = Sequelize.Op,
    Bcrypt = require('../global_functions/Bcrypt');
;

//-- CHECKEA QUE USUARIO ESTA HACIENDO LA PETICION --
function currentUser(req, res) {
    const userActual = Usuario.findOne({
        where: {
            token: req.headers['access-token']
        }
    }).then(user => {
        return user;
    }).catch(err => {
        console.log(err);
        res.status(401).json(new respuestaError(-6, "Sesión expirada"));
    });

    return userActual;
}

//ACTUALIZA LOS DATOS DEL USUARIO ACTUAL
router.put("/", async (req, res) => {

    const userActual = await currentUser(req, res);

    if (userActual !== null) {

        if (comprobarCamposRellenos(req.body, res)) {
            const usuarioUnico = await comprobarUniqueUsuario(req, userActual.id);

            if(usuarioUnico.count === 0){
                actualizarDatos(req, res, userActual.id);
            }else{
                res.status(400).json(new respuestaError(-4, "Usuario ya existente"));  
            }
        }

    } else {
        res.status(400).json(new respuestaError(-2, "No existe el token"));
    }

});

function comprobarCamposRellenos(datos, res) {
    if (datos.nombre === undefined || datos.apellidos === undefined || datos.usuario === undefined || datos.password === undefined) {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    if (datos.nombre.trim() === "" || datos.apellidos.trim() === "" || datos.usuario.trim() === "" || datos.password.trim() === "") {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    return true;
}

function comprobarUniqueUsuario(req, id_usuario) {

    return Usuario.findAndCountAll({
        where: {
            usuario: req.body.usuario,
            id: {
                [Op.ne]: id_usuario
            }
        }
    }).then(result => {
        return result;
    }).catch(error => {
        console.log(error);
    });

}

async function actualizarDatos(req, res, id_usuario) {
    let datos = req.body;

    Usuario.update(
        {
            nombre: datos.nombre,
            apellidos: datos.apellidos,
            usuario: datos.usuario,
            password: await Bcrypt.encriptarPassword(datos.password),
            img_perfil: datos.img_perfil
        },
        {
            where: {
                id: id_usuario
            }
        }).then(() => {
            res.json({message: "Usuario actualizado correctamente"});
        }).catch(error => {
            console.log(error);
        })

}

module.exports = router;