const router = require('express').Router();
const { Card, Comparar, Usuario } = require('../model/dbSequelize');
const respuestaError = require('../model/respuestaError');

//-- CHECKEA QUE USUARIO ESTA HACIENDO LA PETICION --
function currentUser(req, res) {
    const userActual = Usuario.findOne({
        where: {
            token: req.headers['access-token']
        }
    }).then(user => {
        if (user === null) res.status(401).json(new respuestaError(-2, "No existe el token"))
        else return user;
    }).catch(err => {
        console.log(err);
        res.status(401).json(new respuestaError(-6, "Sesión expirada"));
    });

    return userActual;
}

//-- LEE LAS CARTAS QUE HAY EN COMPARAR --
router.get('/', async (req, res) => {

    const userActual = await currentUser(req, res);

    Card.hasMany(Comparar, { foreignKey: 'id_carta' });
    Comparar.belongsTo(Card, { foreignKey: 'id_carta' });

    Comparar.findAll({
        where: {
            id_usuario: userActual.id
        },
        include: [{
            model: Card
        }]
    })
        .then(result => {
            if (result.length > 0) {
                let cartas = result.map(card => card.carta)
                res.json(cartas);
            } else {
                res.json(result);
            }
        })
        .catch(err => {
            console.log(err);
        })

});

//-- AÑADE UNA CARTA A COMPARAR --
router.post('/', async (req, res) => {

    if (comprobarCamposRellenosComparar(req.body, res)) {

        const userActual = await currentUser(req, res);
        const existeCarta = await findCardByIdCardInCards(req.body.idCard);

        if (existeCarta !== null) {

            const existeCartaEnComparar = await comprobarSiCartaYaExisteEnComparar(req.body.idCard, userActual.id);

            if (existeCartaEnComparar === null) {
                Comparar.create({
                    id_carta: req.body.idCard,
                    id_usuario: userActual.id
                }).then(results => {
                    res.json({ message: "Carta añadida a comparar", data: results });
                }).catch(err => {
                    console.log(err);
                });
            } else {
                res.status(400).json(new respuestaError(-4, "Esta carta ya esta añadida"));
            }

        } else {
            res.status(400).json(new respuestaError(-5, "Esta carta no existe"));
        }

    }

});

//-- BORRA UNA CARTA DE TUS COMPARACIONES --
router.delete('/', async (req, res) => {

    if (comprobarCamposRellenosComparar(req.body, res)) {
        const userActual = await currentUser(req, res);
        const existeCarta = await findCardByIdCardInComparar(req.body.idCard, userActual.id);

        if (existeCarta !== null) {
            Comparar.destroy({
                where: {
                    id_carta: req.body.idCard,
                    id_usuario: userActual.id
                }
            }).then(() => {
                res.json({ message: "La carta se ha borrado con exito"});
            })
            .catch(err => {
                console.log(err);
            });   

        } else {
            res.status(400).json(new respuestaError(-5, "La carta que intentas borrar no existe"));
        }
    }

});

function comprobarCamposRellenosComparar(datos, res) {

    if (datos.idCard === undefined) {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    if (datos.idCard.trim() === "") {
        res.status(400).json(new respuestaError(-3, "Faltan datos"));
        return false;
    }

    return true;
}

function findCardByIdCardInComparar(id_card, id_usuario) {
    return Comparar.findOne({
        where: {
            id_carta: id_card,
            id_usuario: id_usuario
        }
    }).catch(err => {
        console.log(err);
    });
}

function findCardByIdCardInCards(id_card) {
    return Card.findOne({
        where: {
            id: id_card
        }
    }).catch(err => {
        console.log(err);
    });
}

function comprobarSiCartaYaExisteEnComparar(id_card, id_usuario) {
    return Comparar.findOne({
        where: {
            id_carta: id_card,
            id_usuario: id_usuario
        }
    }).catch(err => {
        console.log(err);
    });
}

module.exports = router;
