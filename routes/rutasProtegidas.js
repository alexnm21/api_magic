let express = require('express'),
    jwt = require("jsonwebtoken"),
    respuestaError = require('../model/respuestaError'),
    config = require('../configs/config');

const routerProtect = express.Router();

routerProtect.use((req, res, next) => {
    const token = req.headers['access-token'];

    if (token) {
        jwt.verify(token, config.key_token, (err, decoded) => {
            if (err) {
                return res.status(401).json(new respuestaError(-1, "Token no valido"));
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        res.status(401).json(new respuestaError(-2, "Falta token"));
    }
})

module.exports = routerProtect;